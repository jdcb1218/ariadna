<?php

/**
 * @file
 * @author Juan Ceballos
* Primarily Drupal hooks and global API functions.
*/

namespace Drupal\spoty\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\spoty\Controller\SpotyController;

/**
 * Contribute form.
 */

class NewsletterForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'spoty_newsletter_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $statistics = new SpotyController;

     $form['user_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#required' => TRUE,
      '#default_value' =>  \Drupal::state()->get('user_name'),
      '#description' => t('Please writte your name'), 
    );

      $form['email'] = array(
        '#type' => 'email',
        '#required' => TRUE,
        '#title' => $this->t('Email'),
        '#description' => t('Please writte your email'),
        '#default_value' =>  \Drupal::state()->get('email'),
      );
   
     $form['submit'] = array(
       '#type' => 'submit',
       '#value' => t('Subscribe'),
     );

     return $form;
  }

  /**
   * {@submitForm}
   */

  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValues() as $key => $value) {
      \Drupal::state()->set($key,$value);
    }
    // Send Email // To suscribe
     $config = \Drupal::config('system.site');
     $mailManager = \Drupal::service('plugin.manager.mail');
     $params['message'] = t('Hello')  . '&nbsp;'. \Drupal::state()->get('user_name') . '<br>' . t('Thanks for subscribing');
     $params['title'] = t('Thanks for subscribing ') . $config->get('name');
     $langcode = \Drupal::currentUser()->getPreferredLangcode();
     $result = $mailManager->mail('spoty', 'send_newsletter', \Drupal::state()->get('email'), $langcode, $params, NULL, true);
     
     if ($result['result'] !== true) {
       drupal_set_message(t('!!Not sent your subscription'), 'error');
     }
     else {
       drupal_set_message(t('Thx your subscription has been sent'));
     }
  }
}
?>